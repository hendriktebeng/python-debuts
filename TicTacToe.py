from tkinter import *
from tkinter import font
# import time

root = Tk()
root.title("Tic Tac Toe")

turn = "player1"
win = False

def restart():
    global turn, win
    win = False
    upper_left['text'] = ""
    upper_center['text'] = ""
    upper_right['text'] = ""
    middle_left['text'] = ""
    middle_center['text'] = ""
    middle_right['text'] = ""
    lower_left['text'] = ""
    lower_center['text'] = ""
    lower_right['text'] = ""
    turn = "player1"
    player_1['fg'] = "white"
    player_1['bg'] = "red"
    player_2['fg'] = "black"
    player_2['bg'] = "white"
    middle_center['bg'] = "black"

def full():
    if upper_left['text'] != "" and upper_center['text'] != "" and upper_right['text'] != "" and middle_left['text'] != "" and middle_center['text'] != "" and middle_right['text'] != "" and lower_left['text'] != "" and lower_center['text'] != "" and lower_right['text'] != "":
        middle_center['bg'] = "green"
        middle_center['text'] = "DRAW"
        player_1['bg'] = "white"
        player_1['fg'] = "black"
        player_2['bg'] = "white"
        player_2['fg'] = "black"

def win_check():
    global turn, win

    if (upper_left['text'] == upper_center['text'] == upper_right['text'] != "") or (middle_left['text'] == middle_center['text'] == middle_right['text'] != "") or (lower_left['text'] == lower_center['text'] == lower_right['text'] != "") or (upper_left['text'] == middle_left['text'] == lower_left['text'] != "") or (upper_center['text'] == middle_center['text'] == lower_center['text'] != "") or (upper_right['text'] == middle_right['text'] == lower_right['text'] != "") or (upper_left['text'] == middle_center['text'] == lower_right['text'] != "") or (upper_right['text'] == middle_center['text'] == lower_left['text'] != ""):
        win = True
    else:
        full()

    if win == True and turn == "player1":
        middle_center['bg'] = "blue"
        middle_center['text'] = "PLAYER 2 WINS"
        player_1['bg'] = "white"
        player_1['fg'] = "black"
    elif win == True and turn == "player2":
        middle_center['bg'] = "red"
        middle_center['text'] = "PLAYER 1 WINS"
        player_2['bg'] = "white"
        player_2['fg'] = "black"

def on_click(button):
    global turn, win
    if win == True:
        return

    if turn == "player1" and button['text'] == "":
        button['text'] = "X"
        turn = "player2"
        player_1['fg'] = "black"
        player_1['bg'] = "white"
        player_2['fg'] = "white"
        player_2['bg'] = "blue"
    elif turn == "player2" and button['text'] == "":
        button['text'] = "O"
        turn = "player1"
        player_1['fg'] = "white"
        player_1['bg'] = "red"
        player_2['fg'] = "black"
        player_2['bg'] = "white"
    win_check()
    
upper_left = Button(root, text = "", fg = "white", bg = "black", height = 10, width = 25, bd = 7, relief = "raised", command = lambda: on_click(upper_left))
upper_center = Button(root, text = "", fg = "white", bg = "black", height = 10, width = 25, bd = 7, command = lambda: on_click(upper_center))
upper_right = Button(root, text = "", fg = "white", bg = "black", height = 10, width = 25, bd = 7, command = lambda: on_click(upper_right))
middle_left = Button(root, text = "", fg = "white", bg = "black", height = 10, width = 25, bd = 7, command = lambda: on_click(middle_left))
middle_center = Button(root, text = "", fg = "white", bg = "black", height = 10, width = 25, bd = 7, command = lambda: on_click(middle_center))
middle_right = Button(root, text = "", fg = "white", bg = "black", height = 10, width = 25, bd = 7, command = lambda: on_click(middle_right))
lower_left = Button(root, text = "", fg = "white", bg = "black", height = 10, width = 25, bd = 7, command = lambda: on_click(lower_left))
lower_center = Button(root, text = "", fg = "white", bg = "black", height = 10, width = 25, bd = 7, command = lambda: on_click(lower_center))
lower_right = Button(root, text = "", fg = "white", bg = "black", height = 10, width = 25, bd = 7, command = lambda: on_click(lower_right))
restart = Button(root, text = "RESTART", fg = "white", bg = "black", height = 10, width = 25, bd = 7, command  = restart)
player_1 = Label(root, text = "PLAYER 1 (X)", fg = "white", bg = "red", height = 10, width = 25, bd = 7, relief = "raised")
player_2 = Label(root, text = "PLAYER 2 (O)", fg = "black", bg = "white", height = 10, width = 25, bd = 7, relief = "raised")

upper_left.grid(row = 0, column = 0)
upper_center.grid(row = 0, column = 1)
upper_right.grid(row = 0, column = 2)
middle_left.grid(row = 1, column = 0)
middle_center.grid(row = 1, column = 1)
middle_right.grid(row = 1, column = 2)
lower_left.grid(row = 2, column = 0)
lower_center.grid(row = 2, column = 1)
lower_right.grid(row = 2, column = 2)
player_1.grid(row = 3, column = 0)
restart.grid(row = 3, column = 1)
player_2.grid(row = 3, column = 2)

root.mainloop()