import random
# Rock paper scissors game.
# 1 point per win, 3 losses out.

def turn(input):
    input = input.lower()
    num = random.randint(0, 2)
    my_sign = signs[num]
    print("I chose {}.".format(my_sign))
    if (input == 'rock' and my_sign == 'scissors') or (input == 'scissors' and my_sign == 'paper') or (input == 'paper' and my_sign == 'rock'):
        outcome = 1
    elif (my_sign == 'rock' and input == 'scissors') or (my_sign == 'scissors' and input == 'paper') or (my_sign == 'paper' and input == 'rock'):
        outcome = 0
    elif my_sign == input:
        outcome = 2
    else:
        outcome = 3
    return outcome

signs = ['rock', 'paper', 'scissors']
results = ['I win.', 'You win.', 'Tie!']
losses = 0
points = 0

while losses < 3 and points < 3:
    sign = input('Rock, paper or scissors? ')
    output = turn(sign)
    if output == 3:
        print("Your choice is invalid!")
        continue
    result = results[output]
    print(result + "\n")
    if output == 0:
        losses += 1
    elif output == 1:
        points += 1
        print("+ 1 point!")
    print("Losses: {}\nPoints: {}\n".format(losses, points))

if losses == 3:
    print("3 losses, you're out!")
elif points == 3:
    print("3 points, you win!")