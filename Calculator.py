from tkinter import Tk, Entry, Button, PhotoImage
import re

root = Tk()
root.title('Calculator')
root.configure(bg='#2f2f2f')

e = Entry(root, width=40, bg='#2f2f2f', fg='white', borderwidth=5)
e.grid(row=0, column=0, columnspan=4, padx=10, pady=20)
dec_counter = False # The decimal counter is False at startup 
# e.insert(0, '') # this enters a default text inside the textbox

# Regular expressions detector
dec_pattern = re.compile(r'\.')
letter_pattern = re.compile(r'[A-Za-z]')

# Telling the buttons what to do when you press them
def button_click(number):
    e.insert('end', number)

def dec_check():
    global dec_pattern
    entry = e.get()
    if dec_pattern.findall(entry) == ['.']:
        return True
    elif dec_pattern.findall(entry) != ['.']:
        return False

def decimal():
    if dec_check() == False:
        e.insert('end', '.')
    elif dec_check() == True:
        return

def delete():
    e.delete(len(e.get())-1)

def clear():
    global opperation
    opperation  = 0
    e.delete(0, 'end')

def clear_entry():
    e.delete(0, 'end')    

def change_sign():
    num = float(e.get())

    if num > 0:
        e.delete(0, 'end')
        e.insert(0, '-' + str(num))
    elif num < 0:
        e.delete(0, 1)

def addition():
    global first_num
    global opperation
    opperation = 'addition'
    first_num = float(e.get())
    e.delete(0, 'end')

def substract():
    global first_num
    global opperation
    opperation = 'substraction'
    first_num = float(e.get())
    e.delete(0, 'end')

def divide():
    global first_num
    global opperation
    opperation = 'division'
    first_num = float(e.get())
    e.delete(0, 'end')

def multiply():
    global first_num
    global opperation
    opperation = 'multiplication'
    first_num = float(e.get())
    e.delete(0, 'end')

def equal():
    second_num = e.get()
    e.delete(0, 'end')
    if opperation == 'addition':
        result = float(second_num) + first_num
        e.insert(0, result)

    elif opperation == 'substraction':
        result = first_num - float(second_num)
        e.insert(0, result)

    elif opperation == 'multiplication':
        result = float(second_num) * first_num
        e.insert(0, result)

    elif opperation == 'division':
        result = first_num / float(second_num)
        e.insert(0, result)

# Creating the buttons
button_1 = Button(root, text='1', padx=27, pady=15, border=1, bg='#111', fg='white', command=lambda: button_click(1))
button_2 = Button(root, text='2', padx=27, pady=15, border=1, bg='#111', fg='white', command=lambda: button_click(2))
button_3 = Button(root, text='3', padx=27, pady=15, border=1, bg='#111', fg='white', command=lambda: button_click(3))
button_4 = Button(root, text='4', padx=27, pady=15, border=1, bg='#111', fg='white', command=lambda: button_click(4))
button_5 = Button(root, text='5', padx=27, pady=15, border=1, bg='#111', fg='white', command=lambda: button_click(5))
button_6 = Button(root, text='6', padx=27, pady=15, border=1, bg='#111', fg='white', command=lambda: button_click(6))
button_7 = Button(root, text='7', padx=27, pady=15, border=1, bg='#111', fg='white', command=lambda: button_click(7))
button_8 = Button(root, text='8', padx=27, pady=15, border=1, bg='#111', fg='white', command=lambda: button_click(8))
button_9 = Button(root, text='9', padx=27, pady=15, border=1, bg='#111', fg='white', command=lambda: button_click(9))
button_0 = Button(root, text='0', padx=27, pady=15, border=1, bg='#111', fg='white', command=lambda: button_click(1))


button_add = Button(root, text='+', padx=23, pady=15, border=1, bg='#111', fg='white', command=addition)
button_sub = Button(root, text='-', pady=15, padx=24, border=1, bg='#111', fg='white', command=substract)
button_equals = Button(root, text='=', padx=23, pady=15, border=1, bg='#111', fg='white', command=equal)
button_clear_entry = Button(root, text='CE', padx=23, pady=15, border=1, bg='#111', fg='white', command=clear_entry)
button_clear = Button(root, text='C', padx=26, pady=15, border=1, bg='#111', fg='white', command=clear)
button_del = Button(root, text='Delete', padx=14, pady=15, border=1, bg='#111', fg='white', command=delete)
button_multiply = Button(root, text='X', padx=23, pady=15, border=1, bg='#111', fg='white', command= multiply)
button_divide = Button(root, text='/', padx=25, pady=15, border=1, bg='#111', fg='white', command=divide)
button_sign = Button(root, text='+/-', padx=21, pady=15, border=1, bg='#111', fg='white', command=change_sign)
button_decimal = Button(root, text='.', padx=29, pady=15, border=1, bg='#111', fg='white', command=decimal)


# Putting buttons on the screen
button_clear.grid(column=0, row=1)
button_clear_entry.grid(column=1, row=1)
button_del.grid(column=2, row=1)
button_divide.grid(column=3, row=1)

button_7.grid(column=0, row=2)
button_8.grid(column=1, row=2)
button_9.grid(column=2, row=2)
button_multiply.grid(column=3, row=2)

button_4.grid(column=0, row=3)
button_5.grid(column=1, row=3)
button_6.grid(column=2, row=3)
button_sub.grid(column=3, row=3)

button_1.grid(column=0, row=4)
button_2.grid(column=1, row=4)
button_3.grid(column=2, row=4)
button_add.grid(column=3, row=4)

button_sign.grid(column=0, row=5)
button_0.grid(column=1, row=5)
button_decimal.grid(column=2, row=5)
button_equals.grid(column=3, row=5)

root.mainloop() 